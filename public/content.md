# Table
## Description
Tables are data storage units. They have rows and columns to store data. They are defined by a name and a set of columns, with each one having a name, a datatype and a width. Rows store information of records.
**Integrity constraints** are rules specified for each column to declare constraints that need to be followed by each row.
An example of a table, with rows and columns can be seen below.
![Table example](https://docs.oracle.com/cd/B19306_01/server.102/b14220/img/cncpt042.gif)
## Implementation
```
CREATE TABLE table_name (
    <column_name> <datatype>, ...
);
```
## Code Explanation
- `table_name` specifies the name of the table to be created
- each column would have a `column_name` and a `datatype` attribute


# Index
## Descriptions
Indexes are structures associated with tables. They are created by SQL statements executed on a table to be indexed. They speed up data retrieval.
A copy of selected columns of data would be stored from table. These columns would be structured in a way that is very quickly traversable, and they would hold links to the position where the whole row is located.
They help inforce constraints like UNIQUE. Unique indexes ensure that rows do not have duplicate values in a specific column, while nonunique indexes do not.
### Architecture
Indexes can be structured in a **clustered** or **non-clustered** manner.
- **Clustered**: the index would have the same order as in the physical storage of the rows. This would make data retreival very fast.
- **Non-clustered**: as opposed to clustered structure, the physical order of the rows would not match to the one on the index. The indexed columns are usually keys that are not unique constraints like primary-keys

## Implementation
1. with duplicate values allowed
```
CREATE INDEX <index_name> ON <table_name> (<column_name>,...);
```
2. unique indexes
```
CREATE UNIQUE INDEX <index_name> ON <table_name> (<column_name>,...);
```
## Code Explanation
1. creates an index called `<index_name>` on the table `<table_name>`, on the columns specified, like `<column_name>` above.
2. enforces **UNIQUE** constraint on the index creation, hence will not allow for duplicate values.

# View
## Description
A view is a tailored presentation of the data contained in one or more tables or other views. A view takes the output of a query and treats it as a table. Therefore, a view can be thought of as a stored query or a virtual table.
They are similar to tables, with having column attributes and rows. But they are not stored in a storage space like tables, nor do they hold data. They are defined by queries that retrieve data tables.
They enable for tailoring of data for presentation. They also allow access to pre-defined columns and rows. It isolates applications using it from changes done on the original table; they will not be reflected on the view. In views, for presentational purposes, attributes like name of a column can be altered without affecting the original table.

## Implementation
```
CREATE VIEW <view_name> AS SELECT <column_name>,... FROM <table_name> WHERE <condition>;
```
## Code Explanation
Creates a view called `<view_name>` from the statement put next to it; a select statement in the example above.


# Cursor
## Description
A cursor is a databasae object that is used to pinpoint records in a daabase. It points to a specific record that is being used in the database.
As a file is first opened, the cursor points to the fist record. It can, then, be moved to other locations. They are allocated memory and their creation and cleanup needs to be handled well, so as not to hog up memory.

## Implementation
```
CREATE <cursor_name> CURSOR FOR <statement>
```
## Code Explanation
Creates a cursor object for whatever is reterned from the statement specified; e.g. a select statement

# Record
## Description
A record is a row(entry) in a table that is uniquely idenitified using a field like id

## Implementation
```
INSERT INTO <table_name> (<column_name>,...) VALUES (<value>,...);
```
## Code Explanation
Inserts a data into a new row in the table, hence creating a record. 
The data values specified as `<value>` are stored in the corresponding sequential order match of the `<column_name>` attribute in the specified table, `<table_name>`

# Package
## Description
A package is a schema object that groups logically related types, variables, constants, subprograms, cursors, and exceptions. A package is compiled and stored in the database, where many applications can share its contents.
They are useful for creating reusable, reliable code by features like modularity. Modularity refers to encapsulating logically related  objects. Packages can be made easy to understand and well defined. By using interfaces, implementation details can be hidden and be changed without affecting the the interface. They [packages] also prevent unnecessary recompilation 

## Implementation
```
CREATE PACKAGE <package_name> IS <sub_programs | public element declaration> END <package_name>
```
## Code Explanation
in between the `IS` and `END` lies the logic of the package. subprograms that we want to execute or elements we would like to publicall declare can be put


# Procedure
## Description
A procedure is essentially an application program called by __sql call__ statement. It is also called stored procedure.  It is useful to execute sequential statements that do not need a return value.

## Implementation
```
CREATE PROCEDURE <procedure_name> (IN <parameter_name> <data_type> <as_locator> <data> LANGUAGE <language_type> <dml_operation>
```
## Code Explanation
Creates a procedure with input values `parameter_name` and returns (`as_locator`) data (`data`) based on the operations (`dml_operation`) interprated in the language specified (`language_type`).

# Function
## Description
A function is a relationship between a set of input data values and a set of result values. Just like a normal function in programming, a function object deals with specific types of values and returns a specific type of value.

## Implementation
```
CREATE FUNCTION <function_name> (@<parameter_name> <data_type>) RETURNS <return_data_type> AS BEGIN <operation_statements> END RETURN @return <return_variable> END
```
## Code Explanation
Creates a function called `function_name` with `parameter_name` as input, and returns a type `return_data_type` after doing operations (`operation_statements`), which can contain declarations that would be used to hold return values; `@return` in the example above.

# Object
## Description
A databse object is a data structure used to store or reference data. There are a fixed number of database object types. When objects are created, it fits into one of the types.
E.g. a table called customer is an instance of a table object.

## Implementation
creating tables, cursors, and other objects with a specified type would be a suitable implementation example of database objects. 

# DB Link
## Description
It is a connection between two physical database servers that serve as one logical database to a client
These connections are uni-directional, in the sense that server A needs to define a link stored n the data dictionary of B to access database B, and B do the same for them to be able to communicate two ways.
![database link](https://docs.oracle.com/cd/B28359_01/server.111/b28310/img/admin045.gif)
They can be set to be public or private. If it is a public link, all databse users have access to it. If it's private, only the creator of the link has access to it.

## Implementation
```
CREATE <database_link_type> DATABASE LINK <domain> USING <remote_database_name>
```
## Code Explanation
- creates a link between the local database to the one specified as `remote_database_name` located on `domain`.
- `database_link_type` specifies the type, **private**, **public** or **global**

# Sequence 
## Descriptions
Sequence objects are used to sequentially generate numeric values. They are not attached to any table and can be used both indepedently and in INSERT, UPDATE ad DELETE statements.

## Implementation
```
CREATE SEQUENCE <sequence_name> START WITH <init_value> INCREMENT BY <increment_value> MAXVALUE <max_value> <cycle>;
```
## Code Explanation
- Creates a sequence starting from `init_value` to `max_value` with `increment_value` sequential increments.
- `cycle` is an option that specifies what to do after reaching `max_value`
	- `CYCLE` makes the value restart from the beginning
	- `NO CYCLE` discards values exceeding `MAXVALUE`

# Trigger
It is a procedural code that is automatically executed in accordance to events on a table or view in a database.
E.g. 
I. Schema-level triggers: 
	- After Creation
	- Before Alter
	- After Alter
	- Before Drop
	- After Drop
	- Before Insert
There are four main types of triggers.
	1. Row level trigger: executed before or after any column value of row changes
	2. Column level trigger: executed before or aftr a specified column changes.
	3. For each row type: executed once fore each row of theresult set affected by an insert/update/remove
	4. For Each Statement Type: executed only once for the entire result set, but also fires each time the statement is executed.
II. System-level trigger
	- Logon
	- Logoff
	- Startup

## Implementation
```
{CREATE | REPLACE} TRIGGER <trigger_name> {BEFORE | AFTER | INSTEAD OF} <DML_operation> OF <column_name> ON <table_name> WHEN (<condition>)
DECLARE <declaration statement> BEGIN <executable_statement> EXCEPTION <exception_handler_statements> END;
```
## Code Explanation
Creates (`CREATE`) or replaces (`REPLACE`) a trigger called `trigger_name` before (`BEFORE`), after (`AFTER`) or when (`INSTEAD OF`) an operation `DML_operation`, like an insert statement, is done on `column_name` column of the table `table_name`. 
`DECLARE` initializes statement variables that can be referenced during execution of the event handling.
The event handler statements are put after `BEGIN` and go upto `END`. In between, exceptions can be handled by putting the handler statements after `EXCEPTION`

# PRIMARY KEY Constraint
## Description
The PRIMARY KEY constraint uniquely identifies each record in a table. They must contain UNIQUE values, and cannot contain NULL values. A table can have only one primary key, which may consist of single or multiple fields.	

## Implementation
1. 
```
CREATE TABLE <table_name> (<column_name>, ... PRIMARY KEY (<column_name);
```
2. 
```
CREATE TABLE <table_name> (<column_name> <data_type> PRIMARY KEY);
```
## Code Explanation
Both statements create a primary key constraint on the column `column_name`.



# UNIQUE KEY Constraint
## Description
The UNIQUE constraint ensures that all values in a column are different. Both the UNIQUE and PRIMARY KEY constraints provide a guarantee for uniqueness for a column or set of columns. A PRIMARY KEY constraint automatically has a UNIQUE constraint. However, you can have many UNIQUE constraints per table, but only one PRIMARY KEY constraint per table.

## Implementation
```
CREATE TABLE <table_name> (<column_name> <data_type> UNIQUE, ...);
```
## Code Explanation
Creates a unique constraint on `column_name` of the table `table_name`.

# NOT NULL Constraint
## Description
The NOT NULL constraint enforces a column to NOT accept NULL values, which by default allows. This enforces a field to always contain a value, which means that you cannot insert a new record, or update a record without adding a value to this field.

## Implementation
```
CREATE TABLE <table_name> (<column_name> <data_type> NOT NULL, ...);
```
## Code Explanation
Creates a not null constraint on `column_name` of the table `table_name`.

# CHECK Constraint
## Description
The CHECK constraint is used to limit the value range that can be placed in a column. If you define a CHECK constraint on a single column it allows only certain values for this column. If you define a CHECK constraint on a table it can limit the values in certain columns based on values in other columns in the row.

## Implementation
```
CREATE TABLE <table_name> (<column_name> <data type> [<constraint>], CHECK (<check_logic>);
```
## Code Explanation
Creates a check constraint on the column name(s) specified in the `check_logic`, and ensures that requirement be met when inserting records into the column(s).

# FOREIGN KEY Constraint
## Description
A FOREIGN KEY is a key used to link two tables together. It is a field (or collection of fields) in one table that refers to the PRIMARY KEY in another table. The table containing the foreign key is called the child table, and the table containing the candidate key is called the referenced or parent table.

## Implementation
1. 
```
CREATE TABLE <table_name> (<column_name>, ... FOREIGN KEY (<column_name) REFERENCES <foreign_table_name> (<foreign_column_name>);
```
2. 
```
CREATE TABLE <table_name> (<column_name> <data_type> FOREIGN KEY (<column_name) REFERENCES <foreign_table_name> (<foreign_column_name>), ...);
```
## Code Explanation
Both create a foreign key constraint on the column `column_name` and references the key to the column `foreign_column_name` on the table `foreign_table_name`
